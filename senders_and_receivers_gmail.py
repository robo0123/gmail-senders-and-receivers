
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import re
import operator
import logging
import time
from openpyxl import Workbook, load_workbook
from openpyxl.styles import Font
from multiprocessing import Pool
from itertools import repeat, chain
import hashlib
from datetime import timedelta
from socket import timeout

# Configure logging
logging.basicConfig(format='%(name)-8s %(asctime)s - %(levelname)-8s %(message)s',
                    filename='senders_and_receivers_gmail.log',
                    encoding='utf-8', level=logging.NOTSET)


class Sender:
    def __init__(self, name, email):
        self.name = name
        self.email = email

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __hash__(self):
        return hash(('name', self.name,
                     'email', self.email))

    # initialize list of senders from where I received emails
    @staticmethod
    def get_sender_detail(message, service):

        try:
            # Get the message from its id
            msg = service.users().messages().get(
                userId='me', id=message['id']).execute()
            # Get the payload of the msg
            payload = msg['payload']
            # Get the headers of the payload
            headers = payload['headers']
            # Get name and email of sender
            for header in headers:
                if header['name'] == 'From':
                    sender = header['value']
                    sender_name = sender.split('<')[0]
                    sender_email = re.findall('\<(.*?)\>', sender)
                    # Store it in an object
                    try:
                        sender_obj = Sender(sender_name, sender_email[0])
                    except IndexError:
                        logging.error(
                            'Index Error in get_sender_detail occured.')
                        logging.error('sender details: ' + sender)
                        if '@' in sender_name:
                            sender_obj = Sender('', sender_name)
                        else:
                            sender_obj = Sender(sender_name, '')
                    except BaseException as e:
                        logging.error(
                            'Unknown error in get_sender_detail occured.')
                        logging.error(e)
                        sender_obj = Sender('zzunkownerror', 'zzunkownerror')
                    except:
                        logging.error(
                            'Unknown error in get_sender_detail occured.')
                        logging.error('sender details: ' + sender)
                        sender_obj = Sender('zzunkownerror', 'zzunkownerror')
                    return sender_obj
        except timeout:
            logging.error('socket.timeout error occured in get_sender_detail')
            return Sender('zztimeout error', 'zztimeout error')


class Recipient:
    def __init__(self, name, email):
        self.name = name
        self.email = email

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __hash__(self):
        return hash(('name', self.name,
                     'email', self.email))

    # initialize list of recipients who I sent emails to
    @staticmethod
    def get_recipient_detail(message, service):

        try:
            # Get the message from its id
            msg = service.users().messages().get(
                userId='me', id=message['id']).execute()
            # Get the payload of the msg
            payload = msg['payload']
            # Get the headers of the payload
            headers = payload['headers']
            # Get name and email of recipient

            for header in headers:
                if header['name'] == 'To':
                    recipient_header = header['value']
                    recipient_details = recipient_header.split(',')
                    recipient_list = []

                    for detail in recipient_details:
                        if '<' in detail:
                            recipient_name = detail.split('<')[0].strip()
                            recipient_email = re.findall('\<(.*?)\>', detail)
                            recipient_list.append(
                                Recipient(recipient_name, recipient_email[0].strip()))
                        else:
                            recipient_list.append(
                                Recipient('', detail.strip()))

                    return recipient_list
        except timeout:
            logging.error('socket.timeout error occured')
            return list(Recipient('zztimeout error', 'zztimeout error'))
        except BaseException as e:
            logging.error('Unknown error in get_recipient_detail occured.')
            logging.error(e)
            return list(Recipient('zzunkownerror', 'zzunkownerror'))


# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/gmail.readonly']


def main():

    logging.info('main() Started=======================*')
    logging.info('')
    start = time.time()

    """
    More info at: https://developers.google.com/gmail/api
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('gmail', 'v1', credentials=creds)

    # Call the Gmail API (this is from gmail API docs, I just commmented it out)
    '''
    results = service.users().labels().list(userId='me').execute()
    labels = results.get('labels', [])

    if not labels:
        print('No labels found.')
    else:
        print('Labels:')
        for label in labels:
            print(label['name'])
    '''
    # Get user info
    userInfo = service.users().getProfile(userId='me').execute()

    # ======================== HANDLING INBOX ========================
    start_inbox_time = time.time()
    # Get Inbox Messages
    next_page_flag = True

    # gives me dict of email codes
    inbox = service.users().messages().list(userId='me', labelIds=[
        'INBOX'], includeSpamTrash=True).execute()
    # gives me list of messages to be used
    inbox_messages = inbox.get('messages', [])

    print('\nGetting list of emails in INBOX.')
    logging.info('Getting list of emails in INBOX.')

    while next_page_flag:
        if 'nextPageToken' in inbox:
            pt = inbox['nextPageToken']
            inbox = service.users().messages().list(userId='me', labelIds=[
                'INBOX'], includeSpamTrash=True, pageToken=pt).execute()
        else:
            next_page_flag = False
            inbox = service.users().messages().list(userId='me', labelIds=[
                'INBOX'], includeSpamTrash=True).execute()

        inbox_messages += inbox.get('messages', [])

    if not inbox_messages:
        print('\nNo emails found in inbox.')
        logging.info('No emails found in inbox.')
    else:
        print('\n' + str(len(inbox_messages)) +
              ' emails found in inbox. Scanning.')
        logging.info(str(len(inbox_messages)) +
                     ' emails found in inbox. Scanning.')
        received_emails = None
        # multiprocessing to get sender details
        with Pool() as pool:
            received_emails = pool.starmap(
                Sender.get_sender_detail, zip(inbox_messages, repeat(service)))

        logging.info(str(len(received_emails)) +
                     " emails scanned in inbox.")
        print(str(len(received_emails)) + " emails scanned in inbox.")

        unsorted_senders_set = set(received_emails)
        logging.info((str(len(unsorted_senders_set)) + " unique senders."))
        print(str(len(unsorted_senders_set)) + " unique senders.")

        sorted_senders_tup = tuple(
            sorted(unsorted_senders_set, key=operator.attrgetter('email')))

    end_inbox_time = time.time()
    total_inbox_time = timedelta(seconds=(end_inbox_time - start_inbox_time))
    f_total_inbox_time = str(total_inbox_time.total_seconds())
    print('Total time spent in inbox: ' + f_total_inbox_time[:-3] + ' seconds')

    # ========================  HANDLING SENT  ========================
    start_sent_time = time.time()
    # Get Sent Messages
    next_page_flag = True
    sent = service.users().messages().list(userId='me', labelIds=[
        'SENT']).execute()
    sent_messages = sent.get('messages', [])

    print('\nGetting list of emails in SENT.')
    logging.info('Getting list of emails in SENT.')

    while next_page_flag:
        if 'nextPageToken' in sent:
            pt = sent['nextPageToken']
            sent = service.users().messages().list(userId='me', labelIds=[
                'SENT'], pageToken=pt).execute()
        else:
            next_page_flag = False
            sent = service.users().messages().list(userId='me', labelIds=[
                'SENT']).execute()
        
        sent_messages += sent.get('messages', [])

    if not sent_messages:
        print('\nNo emails found in sent.')
        logging.info('No emails found in sent.')
    else:
        print('\nEmails found in sent. Scanning.')
        logging.info('Emails found in sent. Scanning.')
        sent_emails = []
        # multiprocessing to get sender details
        with Pool() as pool:
            sent_emails.extend(pool.starmap(
                Recipient.get_recipient_detail, zip(sent_messages, repeat(service))))

        iter_sent_emails = list(chain.from_iterable(sent_emails))

        logging.info(str(len(iter_sent_emails)) +
                     " emails scanned in sent.")
        print(str(len(iter_sent_emails)) + " emails scanned in sent.")

        unsorted_recipient_set = set(iter_sent_emails)
        logging.info(
            (str(len(unsorted_recipient_set)) + " unique recipients."))
        print(str(len(unsorted_recipient_set)) + " unique recipients.")

        sorted_recipient_tup = tuple(
            sorted(unsorted_recipient_set, key=operator.attrgetter('email')))

    end_sent_time = time.time()
    total_sent_time = timedelta(seconds=(end_sent_time - start_sent_time))
    f_total_sent_time = str(total_sent_time.total_seconds())
    print('Total time spent in sent: ' + f_total_sent_time[:-3] + ' seconds')
    # ======================== HANDLING EXCEL ========================
    print('\nHandling Excel')
    # take note of:
    # sorted_senders_tup
    # sorted_recipient_tup
    email_str = userInfo['emailAddress']
    email_str_hash_obj = hashlib.md5(email_str.encode())
    email_hex_dig = email_str_hash_obj.hexdigest()
    logging.info('Hash made.')

    # get or create xlsx file
    wb_name = 'senders_and_receivers_' + email_hex_dig + '.xlsx'
    wb = None
    try:
        # get workbook
        wb = load_workbook(filename=wb_name)
        logging.info(wb_name + ': opened.')
    except FileNotFoundError:
        # create workbook
        logging.error('FileNotFoundError: ' + wb_name + ' not found.')
        wb = Workbook()
        wb.save(wb_name)
        logging.info(wb_name + ' created.')

    # remove worksheets
    for sheet in wb.sheetnames:
        wb.remove(wb[sheet])
    logging.info(wb_name + ': Sheets removed.')

    # create sheets
    wb.create_sheet('received_from')
    logging.info('Worksheet "received_from" created.')
    wb.create_sheet('sent_to')
    logging.info('Worksheet "sent_to" created.')

    # access sheet named 'received_from' ================
    ws = wb['received_from']
    logging.info('Worksheet "received_from" accessed.')

    # add column names
    ws["A1"] = "Email"
    ws["B1"] = "Name Of Sender"
    logging.info("Column names created.")

    # add entries
    for objs in sorted_senders_tup:
        temp_list = [objs.email, objs.name]
        ws.append(temp_list)
    logging.info("Entries added.")

    # add entry count
    ws["G1"] = "Number of Entries"
    ws["G2"] = len(sorted_senders_tup)
    ws["G3"] = "Gmail Acct"
    ws["G4"] = email_str
    logging.info("Total number of entries is: " + str(len(sorted_senders_tup)))

    # set style
    bold_font = Font(bold=True)
    col_title = [ws["A1"], ws["B1"], ws["G1"], ws["G3"]]
    for title in col_title:
        title.font = bold_font
    logging.info("Font styles set.")

    # access sheet named 'sent_to' ================
    ws = wb['sent_to']
    logging.info('Worksheet "sent_to" accessed.')

    # add column names
    ws["A1"] = "Email"
    ws["B1"] = "Name Of Recipient"
    logging.info("Column names created.")

    # add entries
    for objs in sorted_recipient_tup:
        temp_list = [objs.email, objs.name]
        ws.append(temp_list)
    logging.info("Entries added.")

    # add entry count
    ws["G1"] = "Number of Entries"
    ws["G2"] = len(sorted_recipient_tup)
    logging.info("Total number of entries is: " +
                 str(len(sorted_recipient_tup)))

    # set style
    bold_font = Font(bold=True)
    col_title = [ws["A1"], ws["B1"], ws["G1"]]
    for title in col_title:
        title.font = bold_font
    logging.info("Font styles set.")

    # set filters and sorts
    # ws.auto_filter.add_sort_condition("C2:C" + str(ws.max_row))

    # save workbook
    wb.save(filename=wb_name)
    logging.info("Workbook saved.")

    # print(str(tester))
    # print(hashlib.algorithms_guaranteed)

    # ========================    LAST LOGS    ========================

    end = time.time()
    total_time = timedelta(seconds=(end - start))
    f_total_time = str(total_time.total_seconds())
    print('\nTotal Runtime: ' + f_total_time[:-3] + " seconds\n")

    logging.info('')
    logging.info(f"Runtime of the program is {end - start}")
    logging.info('main() End=======================*')


if __name__ == '__main__':
    main()
